import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    title_mapping = {
        'Mr.': 'Mr.',
        'Mrs.': 'Mrs.',
        'Miss.': 'Miss.'
    }

    median_values = df.groupby(df['Name'].str.extract(r'(\bMr\.|\bMrs\.|\bMiss\.)')[0].map(title_mapping))['Age'].median()

    missing_values = df[df['Age'].isnull()].groupby(df['Name'].str.extract(r'(\bMr\.|\bMrs\.|\bMiss\.)')[0].map(title_mapping))['Age'].size()

    result = [(title, missing_values.get(title, 0), int(round(median_values.get(title, 0))) if not pd.isnull(median_values.get(title)) else 0) for title in title_mapping.values()]

    return result
